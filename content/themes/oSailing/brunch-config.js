// See http://brunch.io for documentation.
exports.files = {
  javascripts: {
    joinTo: {
      'js/app.js': /^app/,
      'js/vendors.js': /^node_modules/,
    }
  },
  stylesheets: {
    joinTo: {
      'css/app.css': /^app/,
      'css/vendors.css': /^node_modules/,
    }
  }
};

exports.plugins = {
  browserSync:{
    files: ['*']
  }
};

exports.watcher = {
  usePolling: true,
  awaitWriteFinish: true
}

exports.npm = {
  styles: {
    'normalize.css': ['normalize.css']
  },
  globals: {
    '$': 'jquery',
    'jQuery': 'jquery',
    'rellax': 'rellax',
    'scrollex': 'jquery.scrollex'
  }
}

exports.modules = {
  autoRequire: {
    'js/app.js': ['initialize']
  }
}
