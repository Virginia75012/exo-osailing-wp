var app = {

  init: function(){

    console.log('init');

    ///// RELLAX /////
    //Je mets en place mon effet rellax sur mon header (intro)
    var photo = new rellax('.header', {speed: 4});

    ///// SCROLLEX /////
    //Je cible mes sections
    $('main > section').each(function(){

      //Pour chaque section...

      // J'isole l'id de mon élément, qui correspond à mon ancre
      var id = $(this).attr('id');

      //$(this) équivaut à ma section
      //J'applique scrollex sur mon élément ( = ma section)
      $(this).scrollex({

        //Je dis à scrollex de se baser sur le milieu de l'élément pour déclencher les événements
        mode: 'middle',

        //Lorsque mon élément entre sur la page (au milieu)
        enter: function(){

          //J'applique la classe "active" sur l'élément "a" de ma sidebar
          $('nav a[href="#'+id+'"]').addClass('active');
        },

        //Lorsque mon élément sort de la page
        leave: function(){

          //Lorsque je quitte l'élément j'enlève la classe "active"
          $('nav a[href="#'+id+'"]').removeClass('active');
        }
      });
    });


  ///// SMOOTHSCROLL /////
  //Je cible tous les liens qui ont une ancre sauf ceux dont les href n'est qu'une ancre
  //(autrement dit les liens avec une ancre & du texte)
  $('a[href*="#"]:not([href="#"])').on('click', app.smoothScroll);
  },

  smoothScroll: function(evt){

    //Je supprime l'événement par défaut de l'ancre
    evt.preventDefault();

    //$(this) est l'élément sur lequel j'ai cliqué
    //(= je récupère mes sections ciblées)
    //Je transforme la cible en objet jQuery
    var target = $(this.hash);

    //Je vérifie bien l'existance de mon élément
    if (target.length){

    //Je viens calculer la position de mon élément par rapport au haut de ma page
    var targetPosition = target.offset().top;

    //Je viens animer le scroll
      $('html, body').animate({
        scrollTop: targetPosition
      }, 1500);
    }

  }
};

$(app.init);